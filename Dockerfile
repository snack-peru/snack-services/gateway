FROM openjdk:8-alpine
LABEL maintainer="Sergio Rivas Medina -> sergiorm96@gmail.com"

ADD ./target/ /app
EXPOSE 8080
WORKDIR /app
ENTRYPOINT ["java", "-jar", "gateway-0.0.1-SNAPSHOT.jar"]
